chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if( request.message === "clicked_browser_action" ) {
        if (document.body.contentEditable=="true") {
            document.body.contentEditable='false'; document.designMode='off'; void 0;
            console.log("Editing mode off!");
        } else {
            document.body.contentEditable='true'; document.designMode='on'; void 0;

            console.log("Editing mode on!");
        }
    }
  }
);